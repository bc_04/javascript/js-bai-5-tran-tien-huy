let main = () => {
  let bai_1 = () => {
    /**
     * Input:
     * - a = 3
     * - b = 10
     * - c = 5
     * - benchmark = 12
     * - location_type_point = 0.5
     * - student_type_point = 1
     * Steps:
     *  - Declare and Initialize 6 variables to store 6 inputs.
     *  - Declare and Initialize a varible "sum" to store total sum
     *  - Declare and Initialize a variable to store output.
     *  - If any of (a,b,c) is 0 => No need to calculate => print out message "Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0"
     *  - Else:
     *      + sum = a+b+c+location_type_point+student_type_point
     *      + Compare sum with benchmark.
     *         => If sum >= benchmark => Đậu
     *         => Else, rớt.
     * Output: Bạn đã đậu. Tổng điểm: 18
     *
     */
    let a = parseInt(document.querySelectorAll(".bai-tap-1 .num-1")[0].value) || 0;
    let b = parseInt(document.querySelectorAll(".bai-tap-1 .num-2")[0].value) || 0;
    let c = parseInt(document.querySelectorAll(".bai-tap-1 .num-3")[0].value) || 0;
    let benchmark = parseInt(document.querySelectorAll(".bai-tap-1 .benchmark")[0].value) || 0;
    let location_type_point = parseInt(document.querySelectorAll(".location-type")[0].value);
    let student_type_point = parseInt(document.querySelectorAll(".student-type")[0].value);
    let output = "Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0";
    let sum = 0;
    a && b && c && 
    (sum = a+b+c+location_type_point+student_type_point) && 
    (output = sum >= benchmark ? `Bạn đã đậu. Tổng điểm: ${sum}` : `Bạn đã rớt. Tổng điểm: ${sum}`);
    document.querySelectorAll(".txt-result-bai-1")[0].innerHTML = output;
  };

  let bai_2 = () => {
    /**
     * Input:
     * - user_name = Huy;
     * - electricity_number = 123
     * Steps:
     *  - Declare and Initialize 3 variables to store 2 inputs.
     *  - Declare and Initialize a variable to store electricity bill.
     *  - Declare and Initialize a variable to store output.
     *  if(electricity_number <= 50) {
     *    electricity_bill = 500 * electricity_number;
     *  } else if(electricity_number <= 100) {
     *    electricity_bill = 650 * (electricity_number - 50) + 500 * 50;
     *  } else if(electricity_number <= 200) {
     *    electricity_bill = 850 * (electricity_number - 100) + 50*650 + 50*500;
     *  } else if(electricity_number <=350) {
     *    electricity_bill = 1100 * (electricity_number - 200) + 100 * 850 + 50*650 + 50*500;
     *  } else {
     *    electricity_bill = 1300 * (electricity_number-350) + 1100 * 150 + 100 * 850 + 50*650 + 50*500;
     *  }
     * 
     *
     * Output: Họ tên: Huy; Tiền điện: 77050
     *
    */
    let user_name = document.querySelectorAll(".user_name")[0].value;
    let electricity_number = parseFloat(document.querySelectorAll(".electricity_number")[0].value) || 0;
    let electricity_bill = 0;
    let output = '';
    
    if(electricity_number <= 50) {
      electricity_bill = 500 * electricity_number;
    } else if(electricity_number <= 100) {
      electricity_bill = 650 * (electricity_number - 50) + 500 * 50;
    } else if(electricity_number <= 200) {
      electricity_bill = 850 * (electricity_number - 100) + 50*650 + 50*500;
    } else if(electricity_number <=350) {
      electricity_bill = 1100 * (electricity_number - 200) + 100 * 850 + 50*650 + 50*500;
    } else {
      electricity_bill = 1300 * (electricity_number-350) + 1100 * 150 + 100 * 850 + 50*650 + 50*500;
    }
    output=`Họ tên: ${user_name}; Tiền điện: ${electricity_bill}`;
    document.querySelectorAll(".txt-result-bai-2")[0].innerHTML = output;
  };

  let bai_3 = () => {
    /**
     * Input:
     * - input_name = Huy;
     * - annual_income = 11,000,000
     * - num_family_members = 0;
     * Steps:
     *  - Declare and Initialize 3 variables to store 3 inputs.
     *  - Declare and Initialize a variable to store money_before_tax.
     *  - Declare and Initialize a variable to store money_after_tax.
     *  - Declare and Initialize a variable to store output.
     *  - Compare the money_before_tax with tax limit to calculate the total money_after_tax
     *
     * Output: Họ tên: huy; Tiền thuế thu nhập cá nhân: 6650000
     *
    */
    let input_name = document.querySelectorAll(".bai-tap-3 .input_name")[0].value || '';
    let annual_income = parseFloat(document.querySelectorAll(".bai-tap-3 .annual_income")[0].value) || 0;
    let num_family_members = parseInt(document.querySelectorAll(".bai-tap-3 .num_family_members")[0].value) || 0;
    let money_before_tax = 0,
        money_after_tax = 0,
      output = ``;
    const TAX_LIMIT_MONEY_1 = 6e7, TAX_LIMIT_RATE_1=0.05;
    const TAX_LIMIT_MONEY_2 = 12e7, TAX_LIMIT_RATE_2=0.1;
    const TAX_LIMIT_MONEY_3 = 21e7, TAX_LIMIT_RATE_3=0.15;
    const TAX_LIMIT_MONEY_4 = 384e6, TAX_LIMIT_RATE_4=0.2;
    const TAX_LIMIT_MONEY_5 = 624e6, TAX_LIMIT_RATE_5=0.25;
    const TAX_LIMIT_MONEY_6 = 96e7, TAX_LIMIT_RATE_6=0.3;
    money_before_tax = annual_income - 4e6 - 16e5 * num_family_members;
    if(money_before_tax <= TAX_LIMIT_MONEY_1) {
      money_after_tax = money_before_tax - (money_before_tax * TAX_LIMIT_RATE_1);
    } else if (money_before_tax <= TAX_LIMIT_MONEY_2) {
      money_after_tax = money_before_tax - ((money_before_tax-TAX_LIMIT_MONEY_1) * TAX_LIMIT_RATE_2) - TAX_LIMIT_MONEY_1 * TAX_LIMIT_RATE_1;
    } else if (money_before_tax <= TAX_LIMIT_MONEY_3) {
      money_after_tax = money_before_tax -
        ((money_before_tax-TAX_LIMIT_MONEY_2) * TAX_LIMIT_RATE_3) -
                          TAX_LIMIT_MONEY_2 * TAX_LIMIT_RATE_2  -
                          TAX_LIMIT_MONEY_1 * TAX_LIMIT_RATE_1;
    } else if(money_before_tax<= TAX_LIMIT_RATE_4) {
      money_after_tax = money_before_tax -
        ((money_before_tax-TAX_LIMIT_MONEY_3) * TAX_LIMIT_RATE_4) -
                          TAX_LIMIT_MONEY_3 * TAX_LIMIT_RATE_3-
                          TAX_LIMIT_MONEY_2 * TAX_LIMIT_RATE_2-
                          TAX_LIMIT_MONEY_1 * TAX_LIMIT_RATE_1;
    } else if(money_before_tax<= TAX_LIMIT_RATE_5) {
      money_after_tax = money_before_tax -
        ((money_before_tax-TAX_LIMIT_MONEY_4) * TAX_LIMIT_RATE_5) -
                          TAX_LIMIT_MONEY_4 * TAX_LIMIT_RATE_4-
                          TAX_LIMIT_MONEY_3 * TAX_LIMIT_RATE_3-
                          TAX_LIMIT_MONEY_2 * TAX_LIMIT_RATE_2-
                          TAX_LIMIT_MONEY_1 * TAX_LIMIT_RATE_1;
    } else if(money_before_tax<= TAX_LIMIT_RATE_6) {
      money_after_tax = money_before_tax -
        ((money_before_tax-TAX_LIMIT_MONEY_5) * TAX_LIMIT_RATE_6) -
                          TAX_LIMIT_MONEY_5 * TAX_LIMIT_RATE_5-
                          TAX_LIMIT_MONEY_4 * TAX_LIMIT_RATE_4-
                          TAX_LIMIT_MONEY_3 * TAX_LIMIT_RATE_3-
                          TAX_LIMIT_MONEY_2 * TAX_LIMIT_RATE_2-
                          TAX_LIMIT_MONEY_1 * TAX_LIMIT_RATE_1;
    } else {
      money_after_tax = money_before_tax -
        ((money_before_tax-TAX_LIMIT_MONEY_6) * 0.35) -
                          TAX_LIMIT_MONEY_6 * TAX_LIMIT_RATE_6-
                          TAX_LIMIT_MONEY_5 * TAX_LIMIT_RATE_5-
                          TAX_LIMIT_MONEY_4 * TAX_LIMIT_RATE_4-
                          TAX_LIMIT_MONEY_3 * TAX_LIMIT_RATE_3-
                          TAX_LIMIT_MONEY_2 * TAX_LIMIT_RATE_2-
                          TAX_LIMIT_MONEY_1 * TAX_LIMIT_RATE_1;
    }
    output = `Họ tên: ${input_name}; Tiền sau khi đóng thu nhập cá nhân: ${money_after_tax}`;
    document.querySelectorAll(".txt-result-bai-3")[0].innerHTML = output;
  };

  let bai_4 = () => {
    /**
     *
     * Input:
     * - customer_type = 2;
     * - customer_id = A;
     * - number_private_channel = 0
     * - number_channels = 1
     * Steps:
     *  - Declare and Initialize 4 variables to store 4 inputs.
     *  - Declare and Initialize a variable to store sum value.
     *  - Declare and Initialize a variable to store output.
     *
     * Output:
     *  - Check condition:
     *     + If:customer_type === 1 => sum = 4.5 + 20.5 + 7.5 * number_private_channel;
     *     + If:customer_type === 2 =>  sum += 15+50*number_private_channel
     *         if(number_channels >10) => sum+= 5*(number_channels-10) + 10 * 75;
     *         else => sum+= 75*number_channels;
     *
     * Output: Mã khách hàng: A; Tiền cáp: $15.00
     *
     */
    let customer_type = parseInt(document.querySelectorAll(".bai-tap-4 .customer-type")[0].value);
    let customer_id = document.querySelectorAll(".bai-tap-4 .customer-id")[0].value;
    let number_private_channel = parseInt(document.querySelectorAll(".bai-tap-4 .numb-private-channels")[0].value) || 0;

    let number_channels = parseInt(document.querySelectorAll(".bai-tap-4 .numb-channels")[0].value) || 0;
    let sum = 0;
    let output = '';
    
    if(customer_type === 1) {
      sum = 4.5 + 20.5 + 7.5 * number_private_channel;
    }
    if(customer_type === 2) {
      sum += 15+50*number_private_channel
      if(number_channels >10) {
        sum+= 5*(number_channels-10) + 10 * 75;
      } else {
        sum+= 75*number_channels;
      }
    }
    output = `Mã khách hàng: ${customer_id};
      Tiền cáp: ${
        new Intl.NumberFormat("en-US", {
          style: "currency",
          currency: "USD"}
        ).format(sum)}`;
    document.querySelectorAll(".txt-result-bai-4")[0].innerHTML = output;
  };


  document
    .querySelectorAll(".btn-calc-bai-1")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_1();
    });

  // giai bai 2
  document
    .querySelectorAll(".btn-calc-bai-2")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_2();
    });

  // giai bai 3
  document
    .querySelectorAll(".btn-calc-bai-3")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_3();
    });

  // giai bai 4
  let customer_type_el = document.querySelectorAll(".bai-tap-4 .customer-type")[0];
  let number_channels_wrapper_el = document.querySelectorAll(".bai-tap-4 .numb-channels-wrapper")[0]; 
  customer_type_el.addEventListener('change', () => {
    number_channels_wrapper_el.style.display = (customer_type_el.value === '2' ? 'block' : 'none');
  })
  document
    .querySelectorAll(".btn-calc-bai-4")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_4();
    });
};

main();